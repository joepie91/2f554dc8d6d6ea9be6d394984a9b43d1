"use strict";

const Promise = require("bluebird");
const AWS = require("aws-sdk");
AWS.config.update({ region: "eu-central-1" });

module.exports = function createRDSInstance(identifier) {
  let rds = new AWS.RDS();
  
  return Promise.try(() => {
    return rds.describeDBInstances({DBInstanceIdentifier: self.identifer}).promise();
  }).then((response) => {
    return {
      rds: rds,
      identifier: identifier,
      instance: response.DBInstances[0],
      otherMethod: function(whatever) {
        return stuff;
      }
    };
  });
};
